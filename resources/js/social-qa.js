const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

window.Vue = require('vue');

import axios from "axios";

if (typeof window.jQuery == 'undefined') {
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
}

const app = new Vue({
    template: '<div></div>' ,
    data: {
        shopifyDomain: '',
        floatingButton: '',
    },
    methods: {
        init() {
            let base = this;
            let url = window.location.href;
            if (url.includes("/")) {
                let params = this.getParams('social-qa');
                this.shopifyDomain = params['shop'];
            }
            var selected_script = document.querySelector('script[src*="/social-qa.js?shop="]');
            var pageURL = window.location.href;
            var dg$;
            var script = document.createElement('script');
            script.setAttribute('src', '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
            script.addEventListener('load', function() {
                dg$ = $.noConflict(true);
                base.getButton(dg$,pageURL,selected_script);
            });
            document.head.appendChild(script);
        },
        getButton(dg$,pageURL,selected_script){
            let len = $('.kinger_analytics_social_qa').length;
            let type = [];
            if (len > 0) {
                $('.kinger_analytics_social_qa').each(function (index, value) {
                    type.push($(this).data('type'));
                });
            } else {
                type = 'undefined';
            }
            console.log(type);
            if(pageURL.indexOf('/products/') > -1){
                let base = this;
                let aPIEndPoint = `${apiEndPoint}/get-button?shop=${base.shopifyDomain}` + `&type=` + type;
                axios({
                    url: aPIEndPoint,
                    method: 'GET',
                    header: {"Access-Control-Allow-Origin": "*"},
                }).then(function (response) {
                    base.addButton(dg$,pageURL,selected_script, type, response.data.data);
                });
            }
        },
        addButton($,pageURL,getScript, btntype, btnresponse){
            let base = this;
            var formData = $(getScript).attr('src').split('?')[1];
            if(pageURL.indexOf('/products/') > -1 ) {
                let proURL = pageURL;
                if(pageURL.indexOf('?') > -1) {
                    proURL = pageURL.split('?')[0];
                }
                axios({
                    url: proURL+'.json',
                    method: 'GET',
                    header: { "Access-Control-Allow-Origin": "*" },
                }).then(function (response) {
                        response = response.data;
                        var type = response.product.product_type, src = '';
                        if(response.product.image) {
                            src = response.product.image.src
                        }

                        // if( $('form[action="/cart/add"]').length ){
                        //     // append Social & QA button
                        //     $('form[action="/cart/add"]').append('<div class="empowerment_btn"><div class="row"><div class="col-md-12 col-12"><a href="#" class="btn btn-success btn-block text-center empowerment_ourBtn" data-toggle="modal" data-target="#bd-example-modal-lg" id="empowerment_ourBtn" type="button">Social Q&A</a> </div> </div></div>');
                        // }
                        if (btntype !== 'undefined')
                            $('.kinger_analytics_social_qa').each(function (index, value) {
                                let t = $(this).data('type');
                                if (t === 'button') {
                                    $(this).html(btnresponse['button']);
                                } else if (t === 'link') {
                                    $(this).html(btnresponse['link']);
                                }
                            });
                        if (btnresponse['floating']) {
                            $("body").prepend(btnresponse['floating']);
                        } else {
                            $("body").prepend(btnresponse['floating']);
                        }
                        // on Social btn click link or button
                        $('body').on('click', '.empowerment_ourBtn', function(){
                            base.getForm(type, 'btn');
                        });
                        // on Submit button lik or button
                        $("#queryForm").click(function() {
                            base.sendForm(response, src, proURL);
                        });

                        // on Social btn click floating
                        $('body').on('click', '.empowerment_ourBtn_floating', function(){
                            base.getForm(type, 'float');
                        });
                        // on Submit button floating
                        $("#queryFormFloat").click(function() {
                            base.sendFormFloating(response, src, proURL);
                        });
                    })

                $('body').on('click', '.empowerment_ourBtn', function(e) {
                    e.preventDefault();
                    var target = $(this).attr('data-target');
                    $(target).show();
                    $('#ourCustomFormFourth').hide();
                });

                $('body').on('click', '.empowerment_ourBtn_floating', function(e) {
                    e.preventDefault();
                    var target = $(this).attr('data-target');
                    $(target).show();
                });

                $('body').on('click', '#success-modal [data-dismiss="modal"]', function(e) {
                    e.preventDefault();
                    $('.custom-control-input:checked').trigger('click');
                    $('#success-modal, #ourCustomFormFourth').hide();
                    $('#ourCustomFormFirst, #ourCustomFormSecond, #ourCustomFormThird').remove();
                });

                $('body').on('click', '#bd-example-modal-lg [data-dismiss="modal"]', function(e) {
                    e.preventDefault();
                    $('.custom-control-input:checked').trigger('click');
                    $('#ourCustomFormFirst, #ourCustomFormSecond, #ourCustomFormThird').remove();
                    $('#bd-example-modal-lg, #ourCustomFormFourth').hide();
                });

                $('body').on('click', '.custom-control-input', function(){
                    if($('.custom-control-input').is(':checked')) {
                        $('.modal-dialog-empow').addClass('expand');
                    } else {
                        $('.modal-dialog-empow').removeClass('expand');
                    }
                });
            }
        },
        getForm(type, btntype){
            let base = this;
            let aPIEndPoint = `${apiEndPoint}/get-token?shop=${base.shopifyDomain}&type=${type}`;
            axios({
                url: aPIEndPoint,
                method: 'POST',
                header: { "Access-Control-Allow-Origin": "*" },
            }).then(function (response) {
                response = response.data;
                if($.trim(response)) {
                    var data = response.data;
                    if(data.error) {
                        console.log(data.error);
                    } else {
                        var $html = '';
                        $.each(data, function(index,value) {
                            var radioCount = 1;
                            if(index === 'csrs') {
                                radioCount = 1;

                                if( btntype == 'btn' ){
                                    $html += '<div class="row" id="ourCustomFormFirst" data-id="retailerCSRToAnser" style="display: none;"><div class="col-sm-12">';
                                }else{
                                    $html += '<div class="csr_ppl_checkbox_main sub_other" style="display: none;">';
                                }
                            }
                            if(index === 'mbrs') {
                                radioCount = 2;
                                if( btntype == 'btn' ){
                                    $html += '<div class="row" id="ourCustomFormSecond" data-id="brandCSRToAnser" style="display: none;"><div class="col-sm-12">';
                                }else{
                                    $html += '<div class="mbr_ppl_checkbox_main sub_other" style="display: none;">';
                                }
                            }
                            if(index === 'experts') {
                                radioCount = 3;
                                if( btntype == 'btn' ){
                                    $html += '<div class="row" id="ourCustomFormThird" data-id="expertOpinion" style="display: none;"><div class="col-sm-12">';
                                }else{
                                    $html += '<div class="expert_ppl_checkbox_main sub_other" style="display: none;">';
                                }
                            }

                            if( btntype == 'btn' ) {
                                $.each(value, function (id, inVal) {
                                    $html += '<div class="box-white">';
                                    $html += '<div class="radiobtnSpacemain">';
                                    $html += '<label class="radiobtnSpace">&nbsp';
                                    $html += '<input type="radio" name="radio' + radioCount + '" value="' + inVal.id + '" >';
                                    $html += '<span class="radiomark"></span>';
                                    $html += '</label>';
                                    $html += '</div>';
                                    $html += '<div class="userpic">';
                                    $html += '<img src="' + inVal.imageUrl + '" class="img-fluid" alt="" />';
                                    $html += '</div>';
                                    $html += '<div class="userNameDes">';
                                    $html += '<h4 class="text-blue">' + inVal.name + '</h4>';
                                    $html += '<p>' + inVal.description + '</p>';
                                    $html += '</div>';
                                    $html += '</div>';
                                });
                            }else{
                               if( value.length > 0 ){
                                   $.each(value, function (id, inVal) {
                                       $html += '<div class="form-check askaq_checkbox">';
                                       $html += '<input class="form-check-input csr_option" type="radio" name="floatradio' + radioCount + '" value="' + inVal.id + '" >';
                                       $html += '<div class="form-check askaq">';
                                       $html += '<img src="' + inVal.imageUrl + '" class="img-fluid" alt="" />';
                                       $html += '</div>';
                                       $html += '<div class="form-group mb-3">';
                                       $html += '<input type="text" class="form-control float-unm" placeholder="" value="' + inVal.name + '" readonly="" aria-label="Username" aria-describedby="basic-addon1">';
                                       $html += '</div></div>';
                                   });
                               }else{
                                   let msg = '';
                                   if(index === 'csrs'){
                                       msg = 'No Customer service Representative available';
                                   }else if( index === 'mbrs' ){
                                       msg = "No Manufacturer's Brand Representative available";
                                   }else{
                                       msg = 'No Expert available';
                                   }
                                   $html += '<div class="form-check askaq_checkbox">';
                                   $html += '<p style="color: #ffffff">' + msg + '</p>';
                                   $html += '</div>';
                               }
                            }
                            $html += '</div>';
                        });
                        if( btntype == 'btn' ) {
                            $('#bd-example-modal-lg .rightForm').prepend($html);
                        }else{
                            $('.screen-3-class .form-content-main').html($html);
                        }
                    }
                }
            });
        },
        sendForm(response, src, proURL){
            let base = this;
            let type = response.product.product_type;
            var question = $("#question").val();
            if (question === "") {
                alert("Please enter your question.");
                return false;
            }
            var name = $("#name").val();
            if (name === "") {
                alert("Please enter your name.");
                return false;
            }
            var preferredModeOfInformation = $("#preferredModeOfInformation").val();
            if (preferredModeOfInformation == "-- Select --") {
                alert("Please select 'How would you like to receive your response'.");
                return false;
            }
            if (preferredModeOfInformation == "Email") {
                var email = $("#email").val();
                if (email === "") {
                    alert("Please enter your email address.")
                    return false;
                }
            }
            if (preferredModeOfInformation == "SMS") {
                var mobileNumber = $("#mobileNumber").val();
                if (mobileNumber === "") {
                    alert("Please enter your mobile number.")
                    return false;
                }
            }
            if (preferredModeOfInformation == "Facebook") {
                var facebookId = $("#facebookId").val();
                if (facebookId === "") {
                    alert("Please enter your facebook id.")
                    return false;
                }
            }
            var brandCSRToAnswer = $("#brandCSRToAnser").is(":checked");
            var retailerCSRToAnser = $("#retailerCSRToAnser").is(":checked");
            var particularExistingCSRToAnser = $("#particularExistingCSRToAnser").is(":checked");
            var expertOpinion = $("#expertOpinion").is(":checked");
            if ((!brandCSRToAnswer) && (!particularExistingCSRToAnser) && (!retailerCSRToAnser) && (!expertOpinion)) {
                alert("Please Select atleast one source to respond to your question.");
                return false;
            }
            if (!$("#term-condition").is(":checked")) {
                alert("Please check the privacy statement and agree.");
                return false;
            }
            var csrId = '' , mbrId = '' , expertId = '';
            if(retailerCSRToAnser) {
                csrId = $('[name="radio1"]:checked').val();
            }
            if(brandCSRToAnswer) {
                mbrId = $('[name="radio2"]:checked').val();
            }
            if(expertOpinion) {
                expertId = $('[name="radio3"]:checked').val();
            }

            let formValues = {
                "question": $("#question").val(),
                "name": $("#name").val(),
                "email": $("#email").val(),
                "sex": $("#sex").val(),
                "isCSR": retailerCSRToAnser,
                "isMBR": brandCSRToAnswer,
                "isExpert": expertOpinion,
                "isVerified": particularExistingCSRToAnser,
                "preferredModeOfComm": $("#preferredModeOfInformation").val(),
                "mobileNumber": $("#mobileNumber").val(),
                "ageGroup": $("#ageGroup").val(),
                "profession": $("#profession").val(),
                "otherProfession": $("#otherProfession").val(),
                "noOfDuration": $("#noOfduration").val(),
                "monthOfDuration": $("#durationOfOwnership").val(),
                "hobbies": $("#hobbies").val(),
                "otherHobbies": $("#otherHobbies").val(),
                "zipCode": $("#zipCode").val(),
                "location": $("#location").val(),
                "state": $("#state").val(),
                "csrId": csrId,
                "mbrId": mbrId,
                "expertId": expertId,
                "productId": response.product.id,
                "productName": response.product.title,
                "sku": response.product.variants[0].sku,
                "productImgUrl": src,
                "productType": type,
                "productPageUrl": proURL
            };

            let aPIEndPoint = `${apiEndPoint}/submit-form?shop=${base.shopifyDomain}&type=${type}`;
            axios({
                url: aPIEndPoint,
                data: {
                    'formValues': formValues,
                },
                method: 'post',
            });
            $("#bd-example-modal-lg, #ourCustomFormFourth").hide();
            $("#success-modal").show();
        },
        sendFormFloating(response, src, proURL){
            let base = this;
            let type = response.product.product_type;

            let asktype = document.querySelector('input[name="question"]:checked').value;
            var question = $("#float_question").val();
            if (question === "") {
                alert("Please enter your question.");
                return false;
            }
            var name = $("#float_unm").val();
            if (name === "") {
                alert("Please enter your name.");
                return false;
            }

            if( asktype == 'receive_phone' ){
                var phone = $("#float_phone").val();
                if (phone === "") {
                    alert("Please enter your mobile number.");
                    return false;
                }
            }
            if( asktype == 'schedule_video' ){
                var email = $("#float_email").val();
                if (email === "") {
                    alert("Please enter your email address.");
                    return false;
                }
            }
            if( asktype == 'schedule_store_visit' ){
                var email = $("#float_email").val();
                if (email === "") {
                    alert("Please enter your email address.");
                    return false;
                }

                var phone = $("#float_phone").val();
                if (phone === "") {
                    alert("Please enter your mobile number.");
                    return false;
                }
            }

            if( asktype == 'ask_a_que' ){
                let askppl = ($("input[name='ans_que']").is(":checked")) ? document.querySelector('input[name="ans_que"]:checked').value : '';
                if ( askppl == '' ) {
                    alert("Please Select atleast one source to respond to your question.");
                    return false;
                }
                let responseType = ($("input[name='response_type']").is(":checked")) ? document.querySelector('input[name="response_type"]:checked').value : '';

                if( responseType == 'Email' && $("#responseEmail").val() == ''){
                    alert("Please enter your email address.");
                    return false;
                }

                if( responseType == 'SMS' && $("#responseMobileNumber").val() == ''){
                    alert("Please enter your mobile number.");
                    return false;
                }

                if( responseType == 'fb' && $("#responseFacebookId").val() == ''){
                    alert("Please enter your facebook id.");
                    return false;
                }

                if( responseType == 'tw' && $("#responseTwitterId").val() == ''){
                    alert("Please enter your twitter id.");
                    return false;
                }

            }

            var brandCSRToAnswer = $("#csr").is(":checked");
            var retailerCSRToAnser = $("#mbr").is(":checked");
            var particularExistingCSRToAnser = $("#verified_customer").is(":checked");
            var expertOpinion = $("#expert").is(":checked");

            var csrId = '' , mbrId = '' , expertId = '';
            if(brandCSRToAnswer) {
                csrId = $('[name="floatradio1"]:checked').val();
            }
            if(retailerCSRToAnser){
                mbrId = $('[name="floatradio2"]:checked').val();
            }
            if(expertOpinion ){
                expertId = $('[name="floatradio3"]:checked').val();
            }
            let formValues = {
                "question": $("#float_question").val(),
                "name": $("#float_unm").val(),
                "email": $("#email").val(),
                "sex": $("#floatsex").val(),
                "isCSR": retailerCSRToAnser,
                "isMBR": brandCSRToAnswer,
                "isExpert": expertOpinion,
                "isVerified": particularExistingCSRToAnser,
                "preferredModeOfComm": document.querySelector('input[name="response_type"]:checked').value,
                "mobileNumber": $("#responseMobileNumber").val(),
                "ageGroup": $("#floatAgeGroup").val(),
                "profession": $("#floatProfession").val(),
                "otherProfession": $("#floatOtherProfession").val(),
                "noOfDuration": $("#floatNoOfduration").val(),
                "monthOfDuration": $("#floatDurationOfOwnership").val(),
                "hobbies": $("#floatHobbies").val(),
                "otherHobbies": $("#floatOtherHobbies").val(),
                "zipCode": $("#zipCode").val(),
                "location": $("#floatLocation").val(),
                "state": $("#state").val(),
                "csrId": csrId,
                "mbrId": mbrId,
                "expertId": expertId,
                "productId": response.product.id,
                "productName": response.product.title,
                "sku": response.product.variants[0].sku,
                "productImgUrl": src,
                "productType": type,
                "productPageUrl": proURL
            };

            let aPIEndPoint = `${apiEndPoint}/submit-form?shop=${base.shopifyDomain}&type=${type}`;
            axios({
                url: aPIEndPoint,
                data: {
                    'formValues': formValues,
                },
                method: 'post',
            });
            $('.other').css("display", "none");
            $('.step-0').css("display", "block");
            $('.back-icon').data("back", 0);
            $('.next-icon').data("next", 1);
            $('.back-icon, .next-icon').data("curr", 0);
            $('.float-title').css("display", 'block');
            $("#bd-float-modal-lg").hide();
            $("#success-modal").show();
        },
        getParams(script_name) {
            // Find all script tags
            var scripts = document.getElementsByTagName("script");
            // Look through them trying to find ourselves
            for (var i = 0; i < scripts.length; i++) {
                if (scripts[i].src.indexOf("/" + script_name) > -1) {
                    // Get an array of key=value strings of params
                    var pa = scripts[i].src.split("?").pop().split("&");
                    // Split each key=value into array, the construct js object
                    var p = {};
                    for (var j = 0; j < pa.length; j++) {
                        var kv = pa[j].split("=");
                        p[kv[0]] = kv[1];
                    }
                    return p;
                }
            }

            // No scripts match

            return {};
        }
    },
    created() {
        this.init();
    },
});

Window.crawlapps_order_delivery = {
    init: function () {
        app.$mount();
    },
};

window.onload = Window.crawlapps_order_delivery.init();

