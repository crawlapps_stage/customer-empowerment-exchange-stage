<?php

namespace App\Http\Middleware;

use Closure;
use Osiset\ShopifyApp\Storage\Models\Charge;

class CheckPlanMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $shop = \Auth::user();
        $curr_date = date('Y-m-d H:i:s');
        $charge = Charge::where('user_id', $shop->id)->where('status', 'ACTIVE')->first();

        if( $charge ){
            $last_date = $charge->cancelled_on;
            if( $curr_date > $last_date ){
                $shop->plan_id = null;
                $shop->save();
            }
        }
        return $next($request);
//        if(is_null($shop->plan_id)){
//            return redirect("/choose-plan");
//        }else{
//            return $next($request);
//        }

    }
}
