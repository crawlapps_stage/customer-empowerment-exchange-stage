<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function test(){
        try{
            $shop = \Auth::user();
            $endpoint = '/admin/api/2020-07/script_tags/128528515131.json';
            $result = $shop->api()->rest('DELETE', $endpoint);
            dd($result);
        }catch(\Exception $e){
            dd($e);
        }
    }
}
