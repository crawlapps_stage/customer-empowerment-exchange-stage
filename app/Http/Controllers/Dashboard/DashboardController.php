<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Response;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        try {
            $shop = Auth::user();
            $parameter['namespace'] = 'Customer_Empowerment';
            $data['key'] = (empty($shop->ka_uuid)) ? '' : $shop->ka_uuid;
            $data['plan_id'] = $shop->plan_id;
//            $result = $shop->api()->rest('GET', 'admin/api/'.env('SHOPIFY_API_VERSION').'/metafields.json', $parameter);
//
//            if ($result['errors']) {
//                return response::json(['data' => 'Their are some error.'], 422);
//            } else {
//                $metafield = $result['body']->container['metafields'];

//            }
            return response::json(['data' => $data], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    public function store(Request $request)
    {
        try {
            $shop = Auth::user();
            $formdata = $request->data;
            $data['key'] = '';

            $result = $shop->api()->rest('GET', 'admin/api/'.env('SHOPIFY_API_VERSION').'/shop.json');
            if ($result['errors']) {
                return response::json(['data' => 'Their are some error.'], 422);
            } else {
                $shopData = $result['body']->container['shop'];
                $uname = explode(' ', $shopData['shop_owner']);
                $address = $shopData['address1'].' '.$shopData['city'].' '.$shopData['country_name'].' '.$shopData['zip'];
                $data = [
                    'userName' => $shopData['myshopify_domain'],
                    'password' => $shop->permenent_password,
                    'name' => $shopData['name'],
                    'firstName' => $uname[0],
                    'lastName' => $uname[1],
                    'companyName' => $shopData['name'],
                    'emailSignature' => $formdata['email'],
                    'smsSignature' => $formdata['sms'],
                    'address' => $address,
                    'email' => $shopData['email'],
                    'phone' => $shopData['phone'],
                    'feature' => 'Type-1'
                ];

                $response = Http::withHeaders([
                    'authorization' => env('KA_AUTHORIZATON'),
                    'Content-Type' => 'application/json'
                ])->post(env('KA_SERVER').'/users', $data);

                $res = $response->body();
                $rdata['key'] = $res;
                $rdata['plan_id'] = $shop->plan_id;
                if ($response->successful()) {
                    // save response in metafield
                    $shop->ka_uuid = $res;
                    $shop->save();

                    $metadata = [
                        "metafield" => [
                            "namespace" => "Customer_Empowerment",
                            "key" => "Customer_Empowerment_Data",
                            "value_type" => "string",
                            "value" => $res
                        ]
                    ];
                    $result = $shop->api()->rest('POST', 'admin/api/'.env('SHOPIFY_API_VERSION').'/metafields.json',
                        $metadata);
                }
            }

            return response::json(['data' => $rdata], 200);
        } catch (\Exception $e) {
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
}
