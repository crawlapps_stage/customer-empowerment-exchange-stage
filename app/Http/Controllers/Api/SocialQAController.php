<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Response;

class SocialQAController extends Controller
{
    public function getToken( Request $request ){
        try{
            $shopDomain = $request->shop;
            $type = $request->type;
            $shop = User::where('name', $shopDomain)->first();

            $response = $this->getBarearToken($shop);

            $res = json_decode($response->getBody());
            if( $response->successful() ){
                $token = $res->access_token;
                $response = Http::withHeaders([
                    'authorization' => 'Bearer ' . $token,
                ])->get(env('KA_SERVER').'/representative', [
                    'category' => $type
                ]);

                $res = json_decode($response->getBody());
            }else{
                $res = [];
            }
            return Response::json(["data" => $res], 200);
        }catch( \Exception $e ){
            return Response::json(["data" => $e], 422);
        }
    }

    public function submitForm( Request $request ){
        try{
            $shopDomain = $request->shop;
            $shop = User::where('name', $shopDomain)->first();

            $response = $this->getBarearToken($shop);
            $res = json_decode($response->getBody());

            if( $response->successful() ){
                $token = $res->access_token;
                $response = Http::withHeaders([
                    'accept' => 'application/json',
                    'authorization' => 'Bearer ' . $token,
                    'content-type' =>  'application/json',
                ])->post(env('KA_SERVER').'/query', $request->formValues);

                $res = json_decode($response->getBody());
            }
            return Response::json(["data" => $res], 200);
        }catch( \Exception $e ){
            return Response::json(["data" => $e], 422);
        }
    }

    public function getBarearToken($shop){
        try{
            $data = [
                'grant_type' => 'password',
                'username' => $shop->name,
                'password' => $shop->permenent_password,
            ];

            $response = Http::withHeaders([
                'authorization' => env('KA_AUTHORIZATON'),
                'Content-Type' => 'application/x-www-form-urlencoded'
            ])->asForm()->post(env('KA_SERVER').'/oauth/token', $data);
            return $response;
        }catch( \Exception $e ){
            return Response::json(["data" => $e], 422);
        }
    }

    public function getButton( Request $request ){
        try{
            $shopDomain = $request->shop;
            $type = ($request->type != "undefined") ? explode(',', $request->type) : 'floating';
            $shop = User::where('name', $shopDomain)->first();

            $style = Setting::where('user_id', $shop->id)->first();
            $style = json_decode($style->style,true);

            if( is_array( $type ) ){
                if( $style['float']['status'] == 1 ){
                    array_push($type, 'floating');
                }
                foreach ( $type as $key=>$val ){
                    $data[$val] = $this->{$val}($style);
                }
            }else{
                $data[$type] = $this->{$type}($style);
            }
            return Response::json(["data" => $data], 200);
        }catch( \Exception $e ){
            return Response::json(["data" => $e], 422);
        }
    }
    public function button($style) {
        $button = $style['button'];
        $shadow = ( $button['shadow_type'] == 'inset' ) ? 'inset' : '';
        $css = "background-color:{$button['bg_color']};color:{$button['text_color']};font-size:{$button['text_size']}px;border-radius:{$button['border_radius']}px;padding:{$button['padding_top']}px {$button['padding_right']}px {$button['padding_bottom']}px {$button['padding_left']}px;border:{$button['border_size']}px solid {$button['border_color']};box-shadow:{$shadow} {$button['shadow_x']}px {$button['shadow_y']}px {$button['shadow_blur']}px {$button['shadow_spread']}px {$button['shadow_color']};cursor: pointer;width: {$button['width']}%;height: {$button['height']}px;display:flex;justify-content:center;align-items: center;";

        $mouseover = "this.style.border='{$button['border_size']}px solid {$button['border_color_hover']}'";
        $mouseout = "this.style.border='{$button['border_size']}px solid {$button['border_color']}'";

        $html = '<a type="button" class="social_qa empowerment_ourBtn" id="social_qa" data-toggle="modal" data-target="#bd-example-modal-lg"' . 'onMouseOver="'.$mouseover.'" onMouseOut="'.$mouseout.'" style="'.$css.'"><span style="">'.$button["label"].'</span></a>';
        return $html;
    }
    public function link($style){
        $style = $style['link'];
        $css = "color:{$style['text_color']};text-decoration:{$style['text_decoration_line']} {$style['text_decoration_style']} {$style['text_decoration_color']};font-size:{$style['text_size']}px;font-weight:{$style['text_weight']};cursor: pointer;display:flex;justify-content:center;align-items: center;";

                $html = '<a type="button" class="social_qa empowerment_ourBtn" id="social_qa" data-toggle="modal" data-target="#bd-example-modal-lg" style="'.$css.'"><span style="">'.$style["label"].'</span></a>';

        return $html;
    }
    public function floating($style){
        $float = $style['float'];
        $shadow = ( $float['shadow_type'] == 'inset' ) ? 'inset' : '';
        $css = "bottom: {$float['bottom_margin']}px;z-index: 9;position: fixed;right: {$float['right_margin']}px;";
        $css .= $float['align'] == 1 ? "" : "left:0;" ;
        $css .= "background-color:{$float['bg_color']};color:{$float['text_color']};font-size:{$float['text_size']}px;border-radius:{$float['border_radius']}px;padding:{$float['padding_top']}px {$float['padding_right']}px {$float['padding_bottom']}px {$float['padding_left']}px;border:{$float['border_size']}px solid {$float['border_color']};box-shadow:{$shadow} {$float['shadow_x']}px {$float['shadow_y']}px {$float['shadow_blur']}px {$float['shadow_spread']}px {$float['shadow_color']};cursor: pointer;width: {$float['width']}px;height: {$float['height']}px;display:flex;justify-content:center;align-items: center;text-align:center;";

        $mouseover = "this.style.border='{$float['border_size']}px solid {$float['border_color_hover']}'";
        $mouseout = "this.style.border='{$float['border_size']}px solid {$float['border_color']}'";
        $html = '<a type="button" class="social_qa empowerment_ourBtn_floating" id="social_qa" data-toggle="modal" data-target="#bd-float-modal-lg"' . 'onMouseOver="'.$mouseover.'" onMouseOut="'.$mouseout.'" style="'.$css.'"><span style="">'.$float["label"].'</span></a>';
        return $html;
    }
}
//empowerment_ourBtn_floating