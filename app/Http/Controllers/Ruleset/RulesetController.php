<?php

namespace App\Http\Controllers\Ruleset;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class RulesetController extends Controller
{
    public function index(Request $request)
    {
        $shop = \Auth::user();

        $parameter['fields'] = 'id,name';
        $sh_themes = $shop->api()->rest('GET', 'admin/api/'.env('SHOPIFY_API_VERSION').'/themes.json', $parameter);
        $theme = [];
        if (!$sh_themes['errors']) {
            $themes = $sh_themes['body']->themes;
            foreach ($themes as $key => $val) {
                $theme[$key]['id'] = $val['id'];
                $theme[$key]['name'] = $val['name'];
                $theme[$key]['is_active'] = ($val->id == $shop->theme_id) ? 1 : 0;
            }
        }
        $curr_theme['id'] = $shop->theme_id;
        $curr_theme['name'] = $shop->theme_name;

        $entity = Setting::where('user_id', $shop->id)->first();
        return \Response::json([
            'themes' => $themes,
            'curr_theme' => $curr_theme,
            'settings' => json_decode(@$entity->style),
        ], 200);
    }

    public function store(Request $request)
    {
        try {
            $shop = \Auth::user();

            $entity = Setting::where('user_id', $shop->id)->first();
            $entity = ($entity) ? $entity : new Setting;
            $entity->user_id = $shop->id;
            $entity->style = json_encode($request->data['settings']);

            $entity->save();

            $theme = $request->data['theme'];
            if ($shop->theme_id != $theme['id']) {
                add_AssetH($theme['id']);
                $shop->theme_id = $theme['id'];
                $shop->theme_name = $theme['name'];
                $shop->save();
            }else{
                $theme_id = $shop->theme_id;
                add_AssetH($theme_id, $shop->id);
            }
            return response(['message' => "Successfully saved."], 200);
        } catch (\Exception $e) {
            return response(['message' => $e->getMessage()], 422);
        }
    }

    public function changeStatus(Request $request){
        try {
            $shop = \Auth::user();
            $entity = Setting::where('user_id', $shop->id)->first();
            if( $entity ){
                $type = $request->type;
                $style = json_decode($entity->style);
                $style->question->$type = !$style->question->$type;
                $entity->style = json_encode($style);
                $entity->save();

                $theme_id = $shop->theme_id;
                add_AssetH($theme_id, $shop->id);
            }
            return response(['message' => "Saved!"], 200);
        } catch (\Exception $e) {
            return response(['message' => $e->getMessage()], 422);
        }
    }
}
