<?php

namespace App\Http\Controllers\Plan;

use App\Http\Controllers\Controller;
use App\Traits\GraphQLTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Osiset\ShopifyApp\Storage\Models\Charge;
use Osiset\ShopifyApp\Storage\Models\Plan;
use Response;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

class PlanController extends Controller
{
   public function index(){
       try{
           $shop = \Auth::user();
           $plan = Plan::select('id', 'name', 'price', 'interval', 'trial_days')->get();
           $data['plan_id'] = $shop->plan_id;
           $data['plan'] = $plan;
           return response::json(['data' => $data], 200);
       }catch( \Exception $e ){
           return response::json(['data' => $e->getMessage()], 422);
       }
   }

   public function changePlan(Request $request){
       try{
           $shop = \Auth::user();
           $db_plan = Plan::where('id', $request->new_plan)->first();
           $price = $request->price;
           $plan = ( $request->plan == 0) ? 'EVERY_30_DAYS' : 'ANNUAL';
           $skey = 'shop_' . $shop->id . '_plan';
           $request->session()->put($skey, $plan);

           $query = 'mutation{
                 appSubscriptionCreate(
                        name: "'. $db_plan->name .'"
                        returnUrl: "'.env('APP_URL').'/change-plan-db"
                        test: true
                        trialDays: '.$db_plan->trial_days.'
                        lineItems: [
                            {
                                plan: {
                                    appRecurringPricingDetails: {
                                        price: { amount: '.$price.', currencyCode: USD },
                                        interval: '.$plan.'
                                    }
                                }
                            }
                        ]
                    ) {
                        appSubscription {
                            id
                        }
                        confirmationUrl
                        userErrors {
                            field
                            message
                        }
                    }
           }';
           $parameters = [];


// Create options for the API
           $options = new Options();
           $options->setVersion('2020-07');

// Create the client and session
           $api = new BasicShopifyAPI($options);
           $api->setSession(new Session(
           $shop->name, $shop->password));

// Now run your requests...
           $result = $api->graph($query, $parameters);
           $data = $result['body']->container['data'];
           return response::json(['data' => $data], 200);
       }catch( \Exception $e ){
           return response::json(['data' => $e->getMessage()], 422);
       }
   }

   public function changePlanDB(Request $request){
       try{
           $shop = Auth::user();
           $old_charge = Charge::where('status', 'ACTIVE')->where('user_id', $shop->id)->first();
           if( $old_charge ){
               $old_charge->status = 'CANCELLED';
               $old_charge->cancelled_on = date('Y-m-d H:i:s');
               $old_charge->save();
           }
           $response = $shop->api()->rest("GET",'/admin/api/'.env('SHOPIFY_API_VERSION').'/recurring_application_charges/'.$request->charge_id);
           if( !$response['errors'] ){
               $skey = 'shop_' . $shop->id . '_plan';
               $value = $request->session()->get($skey);

               $charge_data = $response['body']->container['recurring_application_charge'];
               $cancelled_on = ($value === 'ANNUAL') ? date('Y-m-d', strtotime($charge_data['activated_on']. ' + 365 days')) : date('Y-m-d', strtotime($charge_data['activated_on']. ' + 30 days'));
               $plan = Plan::where('name', $charge_data['name'])->first();

               $planID = $plan->id;
               if( $value == 'ANNUAL'){
                   if( $plan->id == 1 ){
                       $planID = 2;
                   }elseif ( $plan->id == 3  ){
                       $planID = 4;
                   }elseif ( $plan->id == 5  ){
                       $planID = 6;
                   }
               }

               $charge = new Charge;
               $charge->charge_id = $charge_data['id'];
               $charge->test = $charge_data['test'];
               $charge->status = strtoupper($charge_data['status']);
               $charge->name = $charge_data['name'];
               $charge->interval = $value;
               $charge->type = 'RECURRING';
               $charge->price = $charge_data['price'];
               $charge->trial_days = $charge_data['trial_days'];
               $charge->billing_on = date("Y-m-d H:i:s", strtotime($charge_data['billing_on']));
               $charge->activated_on = date("Y-m-d H:i:s", strtotime($charge_data['activated_on']));
               $charge->trial_ends_on = date("Y-m-d H:i:s", strtotime($charge_data['trial_ends_on']));
               $charge->created_at = date("Y-m-d H:i:s", strtotime($charge_data['created_at']));
               $charge->updated_at = date("Y-m-d H:i:s", strtotime($charge_data['updated_at']));
               $charge->cancelled_on = date("Y-m-d H:i:s", strtotime($cancelled_on));
               $charge->plan_id = $planID;
               $charge->user_id = $shop->id;
               $charge->save();

               $shop->plan_id = $planID;
               $shop->save();
           }
           return redirect()->route('home');
       }catch( \Exception $e ){
           return response::json(['data' => $e->getMessage()], 422);
       }
   }

   public function choosePlan(){
       return view('layouts.app');
   }
}
