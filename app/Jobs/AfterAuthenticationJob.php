<?php

namespace App\Jobs;

use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $shop = \Auth::user();
        $shop->permenent_password = ( $shop->permenent_password != '') ?   $shop->permenent_password :  $shop->password;
        $shop->save();

        // add floating button style in setting table
        $userstyle = '{"link": {"label": "Social QA", "text_size": "15", "text_color": "#000000", "text_weight": "inherit", "text_decoration_line": "none", "text_decoration_color": "#000000", "text_decoration_style": "solid"}, "float": {"align": 1, "label": "Social QA", "width": "12", "height": "50", "status": 1, "bg_color": "#ffffff", "shadow_x": "0", "shadow_y": "0", "text_size": "15", "text_color": "#000000", "border_size": "1", "padding_top": "5", "shadow_blur": "20", "shadow_type": "outset", "border_color": "#000000", "padding_left": "5", "right_margin": "0", "shadow_color": "#b2a9a9", "border_radius": "6", "bottom_margin": "0", "padding_right": "5", "shadow_spread": "0", "padding_bottom": "5", "border_color_hover": "#000000"}, "button": {"label": "Social QA", "width": 12, "height": 50, "bg_color": "#ffffff", "shadow_x": "0", "shadow_y": "0", "text_size": 16, "text_color": "#000000", "border_size": "1", "padding_top": "5", "shadow_blur": "20", "shadow_type": "outset", "border_color": "#000000", "padding_left": "5", "shadow_color": "#b2a9a9", "border_radius": 3, "padding_right": "5", "shadow_spread": "0", "padding_bottom": "5", "border_color_hover": "#000000"}, "question": {"ask_a_que": true, "receive_a_phone": true, "schedule_video_call": true, "schedule_store_visit": true, "title": "WELCOME", "bg_color": "#000000", "logo": "https://ucarecdn.com/7e564baf-11f4-41ff-9197-670c0e6ee035/"}}';
        $entity = Setting::where('user_id',$shop->id)->first();
        if( !$entity ){
            $entity = new Setting;
            $entity->user_id = $shop->id;
            $entity->style = $userstyle;
            $entity->save();
        }

        $parameter['role'] = 'main';
        $result = $shop->api()->rest('GET', '/admin/api/'. env('SHOPIFY_API_VERSION') .'/themes.json', $parameter);

        \Log::info(gettype($result['body']));
        \Log::info(json_encode($result['body']));
        $theme_id = $result['body']->themes[0]->id;
        $theme_name = $result['body']->themes[0]->name;

        if( $shop->theme_id == '' ){
            add_AssetH($theme_id);
        }

        $shop->theme_id = $theme_id;
        $shop->theme_name = $theme_name;
        $shop->save();
    }
}
