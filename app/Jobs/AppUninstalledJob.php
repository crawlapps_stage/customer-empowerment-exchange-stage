<?php

namespace App\Jobs;

use App\User;
use Illuminate\Support\Facades\Http;
use Osiset\ShopifyApp\Actions\CancelCurrentPlan;
use Osiset\ShopifyApp\Contracts\Commands\Shop as IShopCommand;
use Osiset\ShopifyApp\Contracts\Queries\Shop as IShopQuery;

class AppUninstalledJob extends \Osiset\ShopifyApp\Messaging\Jobs\AppUninstalledJob
{
    /**
     * Execute the job.
     *
     * @return bool
     */
    public function handle(
        IShopCommand $shopCommand,
        IShopQuery $shopQuery,
        CancelCurrentPlan $cancelCurrentPlanAction
    ): bool {
        \Log::info('------------------- App uninstalled JOB-------------------');
        // Get the shop
        $shop = $shopQuery->getByDomain($this->domain);
        $shopId = $shop->getId();

        // Cancel the current plan
        $cancelCurrentPlanAction($shopId);

        $shop = User::where('name', $shop->name)->first();
        $response = $this->getBarearToken($shop);
        $res = json_decode($response->getBody());

        if( $response->successful() ){
            $token = $res->access_token;
            $response = Http::withHeaders([
                'accept' => 'application/json',
                'authorization' => 'Bearer ' . $token,
                'content-type' =>  'application/json',
            ])->DELETE(env('KA_SERVER').'/users?userName=' . $shop->name);
            \Log::info(json_encode($response));
            if( $response->successful() ){
                // Purge shop of token, plan, etc.
                $shopCommand->clean($shopId);

                // Soft delete the shop.
                $shopCommand->softDelete($shopId);

                $shop->ka_uuid = '';
                $shop->permenent_password = '';
                $shop->plan_id = null;
                $shop->save();
            }
            \Log::info(json_encode($response));
            $res = json_decode($response->getBody());
            \Log::info(json_encode($res));
        }
        return true;
    }

    public function getBarearToken($shop){
        try{
            $data = [
                'grant_type' => 'password',
                'username' => $shop->name,
                'password' => $shop->permenent_password,
            ];

            $response = Http::withHeaders([
                'authorization' => env('KA_AUTHORIZATON'),
                'Content-Type' => 'application/x-www-form-urlencoded'
            ])->asForm()->post(env('KA_SERVER').'/oauth/token', $data);
            return $response;
        }catch( \Exception $e ){
            return Response::json(["data" => $e], 422);
        }
    }
}
