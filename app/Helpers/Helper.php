<?php

use App\Models\Setting;

if (!function_exists('getThemeH')) {
    /**
     * @return mixed
     */
    function getThemeH()
    {
        try {
            $shop = \Auth::user();
            $parameter['role'] = 'main';
            $result = $shop->api()->rest('GET', '/admin/api/'.env('SHOPIFY_API_VERSION').'/themes.json', $parameter);

            \Log::info(gettype($result['body']));
            \Log::info(json_encode($result['body']));
            $theme_id = $result['body']->themes[0]->id;
            return $theme_id;
        } catch (\Exception $e) {
            \Log::info('====== ERROR:: get theme helper error =====');
        }
    }
}

if (!function_exists('add_AssetH')) {
    /**
     * @param $theme_id
     */
    function add_AssetH($theme_id, $shop_id)
    {
        try {
            $entity = Setting::where('user_id', $shop_id)->first();
            $style = json_decode(@$entity->style);
            $style = $style->question;

            addJSAsset($theme_id);
            addCSSAsset($theme_id, $style);
            addSnippetH($theme_id, $style);
        } catch (\Exception $e) {
            \Log::info('====== ERROR:: get theme helper error =====');
        }
    }
}

if (!function_exists('addJSAsset')) {
    /**
     * @param $theme_id
     */
    function addJSAsset($theme_id)
    {
        try {
            $shop = \Auth::user();
            $scriptCode = getScriptCode();

            $parameter['asset']['key'] = 'assets/customer_empowerment.js';
            $parameter['asset']['value'] = $scriptCode;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

        } catch (\Exception $e) {
            \Log::info('====== ERROR:: addJSAsset error =====');
        }
    }
}

if (!function_exists('getScriptCode')) {
    /**
     * @return string
     */
    function getScriptCode()
    {
        // Asset customerEmpowerment JS
        $js_code = 'jQuery(document).ready(function($) {
                    $("#ourCustomFormFirst").css("display", "none");
                    $("#ourCustomFormSecond").css("display", "none");
                    $("#ourCustomFormThird").css("display", "none");
                    $("#ourCustomFormFourth").css("display", "none");
                    $("#selectionview").css("display", "none");
                    $("#custombox1").css("display", "none");
                    $("#custombox2").css("display", "none");
                    $("#custombox3").css("display", "none");
                    $("#custombox4").css("display", "none");
                    $("#phoneId").css("display", "none");
                    $("#emailId").css("display", "none");
                    $("#otherProfession").css("display", "none");
                    $("#otherHobbies").css("display", "none");

                    $("#noOfduration").attr({
                        "max" : 31, // substitute your own
                        "min" : 1
                        // values (or variables) here
                    });
                    $("#profession").change(function() {
                      var value = $("option:selected", this).text();
                      if (value == "Other") {
                        $("#otherProfession").css(
                            "display", "block");
                        } else {
                        $("#otherProfession").css(
                            "display", "none");
                        }
                    });
                    $("#hobbies").change(function() {
                        var value = $("option:selected", this).text();
                        if (value == "Other") {
                            $("#otherHobbies").css("display", "block");
                        } else {
                            $("#otherHobbies").css("display", "none");
                        }
                    });
                    $("#durationOfOwnership").change(function() {
                        var value = $("option:selected", this).text();
                        if (value == "Days") {
                          $("#noOfduration").attr({
                            "max" : 31, // substitute your own
                            "min" : 1
                            // values (or variables) here
                          });
                        }
                        if (value == "Weeks") {
                          $("#noOfduration").attr({
                            "max" : 4, // substitute your own
                            "min" : 1
                            // values (or variables) here
                          });
                        }
                        if (value == "Years") {
                          $("#noOfduration").attr({
                            "max" : 20, // substitute your own
                            "min" : 1
                            // values (or variables) here
                          });
                        }
                        if (value == "Months") {
                          $("#noOfduration").attr({
                            "max" : 12, // substitute your own
                            "min" : 1
                            // values (or variables) here
                          });
                        }
                    });
                    $("#retailerCSRToAnser, #brandCSRToAnser, #expertOpinion, #particularExistingCSRToAnser").click(function(event) {
                        showFormDetails(event);
                    });
                    $("#custombox1 , #custombox2, #custombox3, #custombox4").click(function(event) {
                        showFormOnButton(event);
                    });
                });

                function showFormDetails(event) {
                    var retailerCSRToAnser = $("#retailerCSRToAnser").is(":checked");
                    var brandCSRToAnser = $("#brandCSRToAnser").is(":checked");
                    var expertOpinion = $("#expertOpinion").is(":checked");
                    var particularExistingCSRToAnser = $("#particularExistingCSRToAnser").is(":checked");
                    $("#selectionview").hide();
                    $("#custombox1, #custombox2, #custombox3, #custombox4").hide();
                    $("#ourCustomFormFirst, #ourCustomFormSecond, #ourCustomFormThird, #ourCustomFormFourth").hide();
                    var count = 0;
                    if (retailerCSRToAnser) {
                        count++;
                    }
                    if (brandCSRToAnser) {
                        count++;
                    }
                    if (expertOpinion) {
                        count++;
                    }
                    if (particularExistingCSRToAnser) {
                        count++;
                    }

                    if (count == 1) {
                        if (retailerCSRToAnser) {
                            $("#ourCustomFormFirst").show("toggle");
                        }
                        if (brandCSRToAnser) {
                            $("#ourCustomFormSecond").show("toggle");
                        }
                        if (expertOpinion) {
                            $("#ourCustomFormThird").show("toggle");
                        }
                        if (particularExistingCSRToAnser) {
                            $("#ourCustomFormFourth").show("toggle");
                        }
                    }
                    if (count > 1) {
                        // show  hide form here.
                        $("#selectionview").show();
                        if (retailerCSRToAnser) {
                          showCSRForm();
                        }
                        if (brandCSRToAnser) {
                          showBRSForm();
                        }
                        if (expertOpinion) {
                          showExpertForm();
                        }
                        if (particularExistingCSRToAnser) {
                          showExistForm();
                        }
                        var currentSelectId = event.target.id;
                        if (currentSelectId == "retailerCSRToAnser") {
                          if (retailerCSRToAnser) {
                            showCSRForm();
                          }
                          $("#custombox1").prop("disabled", true);
                        } else if (currentSelectId == "brandCSRToAnser") {
                          if (brandCSRToAnser) {
                            showBRSForm();
                          }
                          $("#custombox2").prop("disabled", true);
                        } else if (currentSelectId == "expertOpinion") {
                          if (expertOpinion) {
                            showExpertForm();
                          }
                          $("#custombox3").prop("disabled", true);
                        } else {
                          if (particularExistingCSRToAnser) {
                            showExistForm();
                          }
                          $("#custombox4").prop("disabled", true);
                        }
                        var dataId = $(".rightForm div:visible").attr("data-id");
                        $("#bd-example-modal-lg button[data-id=\'"+dataId+"\']").attr("disabled", true);
                    }

                    function showCSRForm() {
                        $("#ourCustomFormFirst").show("toggle");
                        $("#ourCustomFormSecond").hide();
                        $("#ourCustomFormThird").hide();
                        $("#ourCustomFormFourth").hide();
                        $("#custombox1").prop("disabled", false);
                        $("#custombox1").show();
                    }
                    function showBRSForm() {
                        $("#ourCustomFormFirst").hide();
                        $("#ourCustomFormSecond").show("toggle");
                        $("#ourCustomFormThird").hide();
                        $("#ourCustomFormFourth").hide();
                        $("#custombox2").prop("disabled", false);
                        $("#custombox2").show();
                    }
                    function showExpertForm() {
                        $("#ourCustomFormFirst").hide();
                        $("#ourCustomFormSecond").hide();
                        $("#ourCustomFormThird").show("toggle");
                        $("#ourCustomFormFourth").hide();
                        $("#custombox3").prop("disabled", false);
                        $("#custombox3").show();
                    }

                    function showExistForm() {
                        $("#ourCustomFormFirst").hide();
                        $("#ourCustomFormSecond").hide();
                        $("#ourCustomFormThird").hide();
                        $("#ourCustomFormFourth").show("toggle");
                        $("#custombox4").prop("disabled", false);
                        $("#custombox4").show();
                    }
                }

                function showFormOnButton(event) {
                    $("#custombox1").prop("disabled", false);
                    $("#custombox2").prop("disabled", false);
                    $("#custombox3").prop("disabled", false);
                    $("#custombox4").prop("disabled", false);
                    $("#ourCustomFormFirst, #ourCustomFormSecond, #ourCustomFormThird, #ourCustomFormFourth").hide();
                    var buttonId = event.target.id;
                    if (buttonId == "custombox1") {
                        $("#ourCustomFormFirst").show();
                        $("#custombox1").prop("disabled", true);
                    } else if (buttonId == "custombox2") {
                        $("#ourCustomFormSecond").show();
                        $("#custombox2").prop("disabled", true);
                    } else if (buttonId == "custombox3") {
                        $("#ourCustomFormThird").show();
                        $("#custombox3").prop("disabled", true);
                    } else if (buttonId == "custombox4") {
                        $("#ourCustomFormFourth").show();
                        $("#custombox4").prop("disabled", true);
                    }
                }

                function modeOfCommunication(value) {
                    if (value.value == "SMS") {
                        $("#emailId").hide();
                        $("#phoneId").show();
                        $("#facebookId").hide();
                    }
                    if (value.value == "Email") {
                        $("#emailId").show();
                        $("#phoneId").hide();
                        $("#facebookId").hide();
                    }
                    if (value.value == "-- Select --") {
                        $("#emailId").hide();
                        $("#phoneId").hide();
                        $("#facebookId").hide();
                    }
                    if (value.value == "Facebook") {
                        $("#facebookId").show();
                        $("#emailId").hide();
                        $("#phoneId").hide();
                    }
                }

                function formShow(value) {
                    if (value.value === "Yes") {
                        $("#personFormId").show();
                    } else {
                        $("#personFormId").hide();
                    }
                }

                jQuery(document).ready(function($) {
                    $("#phoneId").hide();
                    $("#facebookId").hide();
                    $("#emailId").hide();
                });';

        $js_code .= '  //   start :: float model js
        jQuery(document).ready(function($) {
            //   close popup
            $("#float_close").click(function() {
              floatModelInit();
              $("#bd-float-modal-lg").fadeOut();
            });
            
              //   home option select
            $(".home_option").click(function() {
                nextBack("next", true);
            });
            $(".ask_a_que_option").click(function() {
                let opt = $(this).data("value");
                $(".ask_a_que_option_value").val(opt);
                askoption();
                nextBack("next", false);
            });
            $(".response_option").click(function() {
                  let opt = $(this).val();
                  $(".response_opt").css("display", "none");
                  if( opt == "Email" ){
                     $("#fEmailId").css("display", "block");
                  }	else if( opt == "SMS" ){
                    $("#fPhoneId").css("display", "block");
            }	else if( opt == "fb" ){
                $("#fFacebookId").css("display", "block");
            }	else if( opt == "tw" ){
                $("#fTwitterId").css("display", "block");
            }
        });
        $(".back-icon").click(function() {
            nextBack("back", true);
        });
        $(".next-icon").click(function() {
            nextBack("next", true);
        });
        //   end :: float model js
        });
        
        //   start :: float model js function
        function askoption(){
            let opt = $(".ask_a_que_option_value").val();
           $(".sub_other").css("display", "none");
           if( opt == "CSR" ){
               $(".csr_ppl_checkbox_main").css("display", "block");
           } else if( opt == "MBR" ){
               $(".mbr_ppl_checkbox_main").css("display", "block");
           } else if( opt == "expert" ){
               $(".expert_ppl_checkbox_main").css("display", "block");
           } else if( opt == "verified_customer" ){
               $(".verified_ppl_checkbox_main").css("display", "block");
           }
        }
        
        function nextBack(action, ifCallFloatFun){
            let bkstep = $(".back-icon").data("back");
            let nextstep = $(".next-icon").data("next");
        
            $(".other").css("display", "none");
        
            let acstep = ( action == "next" ) ? nextstep : bkstep;
        
            $(".step-" + acstep).css("display", "block");
            $(".back-icon").data("back", (acstep - 1));
            $(".next-icon").data("next", (acstep + 1));
        
            let curr = ( acstep < 0  ) ? 0 : acstep;
            $(".back-icon, .next-icon").data("curr", curr);
            let currstep = $(".back-icon").data("curr");
        
            ( currstep == 0 ) ? $(".back-icon").css("display", "none") : $(".back-icon").css("display", "block");
            ( currstep == 0 ) ? $(".float-title").css("display", "block") : $(".float-title").css("display", "none");
            ( currstep == 0 ) ? $(".float-sub-title").html("Ask a question") : "";
        
        //   	( action == "next" ) ? floatModelSlide() : "";
            (ifCallFloatFun) ? floatModelSlide() : "";
        
            if( currstep == 3 ){
                askoption();
            }
        }
        
        function floatModelSlide(){
            let homeOpt = $("input[name=\'question\']:checked").val();
            let subTitleHtml = "";
            if( homeOpt == "ask_a_que" ){
                $(".float-email").css("display", "none");
                $(".float-phone").css("display", "none");
                subTitleHtml = "Ask a question";
            } else if ( homeOpt == "receive_phone" ){
                $(".float-phone").css("display", "block");
                $(".float-email").css("display", "none");
                subTitleHtml = "Receive a phone call";
            } else if ( homeOpt == "schedule_video" ){
                $(".float-email").css("display", "block");
                $(".float-phone").css("display", "none");
                subTitleHtml = "Schedule a video call";
            } else if ( homeOpt == "schedule_store_visit" ){
                $(".float-email").css("display", "block");
                $(".float-phone").css("display", "block");
                subTitleHtml = "Schedule a showroom visit";
            }
            let curr = $(".back-icon").data("curr");
            if( homeOpt == "receive_phone" ||  homeOpt == "schedule_video" ||  homeOpt == "schedule_store_visit"){
                let label1html = "";
                let label2html = "";
                if( curr == 2 ){
                    if( homeOpt == "receive_phone" ){
                        label1html = "Please give us two convenient days and times you are available to take the phone call";
                        label2html = "You\'ll receive a confirmation msg & a 15 minutes before the call reminder text msg on your mobile phone.";
                    } else if ( homeOpt == "schedule_video" ){
                        label1html = "Please give us two convenient days and times you are available to take the video call";
                        label2html = "You\'ll receive an email invite for one   of your available day & time with name of Rep on video call with you.";
                    } else if ( homeOpt == "schedule_store_visit" ){
                        label1html = "Please give us two convenient days and times you are available to visit our showroom";
                        label2html = "You\'ll receive an email invite for one of your available day & time with name of Rep who will be with you during your visit. We\'ll send you a reminder text msg 30 minutes before you pay us a visit.";
                    }
                    $(".step-2-label-1").html(label1html);
                    $(".step-2-label-2").html(label2html);
        
                    $(".next-arrow-main").css("display", "none");
                    $(".submit-btn-main").css("display", "block");
                    $(".receive_phone_s2").css("display", "block");
                    $(".ask_a_que_s2").css("display", "none");
                }else{
                    $(".next-arrow-main").css("display", "block");
                    $(".submit-btn-main").css("display", "none");
                }
            }else{
                $(".next-arrow-main").css("display", "block");
                $(".submit-btn-main").css("display", "none");
        
                if( curr == 2 ){
                    $(".ask_a_que_s2").css("display", "block");
                    $(".receive_phone_s2").css("display", "none");
                }
            }
            if( curr == 4 ){
                $(".next-arrow-main").css("display", "none");
                $(".submit-btn-main").css("display", "block");
            }
            $(".float-sub-title").html(subTitleHtml);
        }
        
        function floatModelInit(){
            $(".other").css("display", "none");
            $(".step-0").css("display", "block");
            $(".back-icon").data("back", 0);
            $(".next-icon").data("next", 1);
            $(".back-icon, .next-icon").data("curr", 0);
            $(".float-title").css("display", "block");
        }
        //   end :: float model js function';

        return $js_code;
    }
}

if (!function_exists('addCSSAsset')) {
    /**
     * @param $theme_id
     */
   function addCSSAsset($theme_id, $style){
        try {
            \Log::info('-----------------------START :: addCSSAsset -----------------------');
            $shop = \Auth::user();
            $cssCode = getCSSCode($style);

            $parameter['asset']['key'] = 'assets/customer_empowerment.css';
            $parameter['asset']['value'] = $cssCode;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            \Log::info(json_encode($asset));
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addCSSAsset -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('getCSSCode')) {
    /**
     * @return string
     */
    function getCSSCode($style)
    {
        // Asset customerEmpowerment JS
        $css_code = '@import url(https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700,800&display=swap);#bd-example-modal-lg{display:block;position:fixed;width:100%;height:100%;top:0;left:0;bottom:0;right:0;background:rgba(0,0,0,.6);z-index:9999}#bd-example-modal-lg .modal-dialog-empow{position:relative;top:50%;left:50%;transform:translate(-50%,-50%);background:#fff}.empowerment_btn{width:100%}.empowerment_btn .btn{width:100%;text-transform:none}.header{position:fixed;height:70px;width:100%;left:0;top:0;z-index:123}.header-right{float:right;width:calc(100% - 250px);background:#fff;height:70px;-webkit-box-shadow:0 0 6px rgba(0,0,0,.4);box-shadow:0 0 6px rgba(0,0,0,.4)}.header-right .brand-logo{float:left;margin-right:10px;width:70px;display:none}.header-right .brand-logo a{padding:0 6px 0}.header-right .menu-icon{position:relative;right:0;float:left;display:none}.user-info-dropdown{float:right;padding:10px 20px 10px 0}.user-info-dropdown .dropdown-toggle{display:block;padding:10px 0;font-size:16px}.user-info-dropdown .dropdown-toggle .user-icon{width:32px;height:32px;border:1px solid #0288d1;color:#0288d1;line-height:30px;text-align:center;display:inline-block;vertical-align:middle;border-radius:100%}.user-info-dropdown .dropdown-toggle .user-name{font-weight:400;display:inline-block;vertical-align:middle;margin-left:5px;color:#131e22;font-family:"Work Sans",sans-serif}.user-info-dropdown .dropdown-item{position:relative;padding-left:45px}.user-info-dropdown .dropdown-item .fa{position:absolute;left:16px;top:11px;font-size:18px}.user-notification{float:right;margin-right:30px;padding:22px 20px 10px 0}.user-notification .dropdown-toggle{font-size:20px;padding:10px;color:#91a8b0;position:relative}.user-notification .dropdown-toggle .badge{position:absolute;right:8px;top:10px;background:#f5678a;width:5px;height:5px;display:block;padding:0}.user-notification .dropdown-menu{width:340px}.notification-list ul li:nth-child(2n){background:#eff5f7}.notification-list ul li a{display:block;position:relative;padding:10px 15px 10px 75px;min-height:75px;color:#33484f;font-size:15px;font-family:"Work Sans",sans-serif}.notification-list ul li a img{width:50px;height:50px;position:absolute;left:10px;top:13px;border-radius:100%;-webkit-box-shadow:0 0 4px rgba(0,0,0,.5);box-shadow:0 0 4px rgba(0,0,0,.5)}.notification-list ul li a h3{font-size:18px;color:#33484f;font-weight:500;font-family:"Work Sans",sans-serif}.notification-list ul li a h3 span{float:right;font-size:14px;font-weight:500;padding-top:2px}.notification-list ul li a p{margin-bottom:0}.footer-wrap{width:100%;position:relative;text-align:center;font-weight:500}.modal-dialog-empow.expand{width:100%!important}.modal-dialog-empow{padding:0;border-radius:.3rem}.modal-dialog-empow .modal-body .form-control{height:30px!important;padding:5px 10px}.modal-dialog-empow .modal-body label{margin-bottom:12px!important}.modal-body .col-sm-7.rightForm{width:57%!important;float:right;padding:15px}.modal-dialog-empow .mainform{width:100%!important;max-width:340px;float:left;padding:15px 15px 0}.modal-content{display:inline-block;width:100%}.modal-content .modal-body{display:inline-block;width:100%}.modal-body .form-group.required label.custom-control-label{margin-bottom:0!important}.modal-dialog-empow .modal-footer button{padding:9px 10px;line-height:12px;font-weight:600;letter-spacing:.2px;font-size:11px}.modal-footer .form-button-row{text-align:left}.modal-footer .form-button-row button{max-width:130px;width:100%;height:40px;background-color:#007bff}.modal-footer .form-button-row button.btn{background-color:#007bff;font-size:10px;height:40px;line-height:10px}.modal-content .modal-body{padding-bottom:0}.modal-content .modal-body{padding-bottom:0;padding-top:0}.modal-body input,.modal-body select,.modal-body textarea{width:100%;border-radius:4px}.modal-body input[type=checkbox]{width:auto}.modal-footer .border-top{padding-top:8px}.modal-dialog-empow .modal-body label{margin-bottom:4px!important;margin-top:6px}.modal-body .rightForm .no-gutters input[type=number]{margin-bottom:10px}.modal-footer .form-button-row{padding:0 20px}.modal-footer{padding-bottom:20px}.modal-dialog-empow label{font-weight:400}.modal-dialog-empow .modal-footer button#queryForm{background-color:#007bff}.modal-dialog-empow .custom-checkbox input[type=checkbox]{opacity:0!important;visibility:hidden!important;margin-right:0;display:none}.custom-checkbox input[type=checkbox]+label{background:url(https://kingeranalytics-demo.com/img/checkbox.jpg) no-repeat!important;width:auto!important;height:20px!important;display:inline-block!important;padding-left:27px;overflow:visible!important}.custom-checkbox input[type=checkbox]:checked+label{background:url(https://kingeranalytics-demo.com/img/checkbox-checked.jpg) no-repeat!important}.modal-content .rightForm .no-gutters .col-sm-9{width:75%;display:inline-block}.modal-content .rightForm .no-gutters .col-sm-3{width:25%;display:inline-block;float:left}.modal-body .rightForm #personFormId .row .col-sm-6.col-xs-12{width:49%;display:inline-block}.modal-body .rightForm #personFormId label{padding-left:0!important}.modal-dialog-empow .mainform textarea#question{min-height:75px}.modal-dialog-empow .modal-body{padding:0 5px}.modal-content button.btn{text-transform:capitalize;font-size:14px;font-weight:200;background-color:#6c757d;letter-spacing:.5px;border-radius:4px}.modal-footer label.checkboxSpace.footercheckbox{margin-left:5px}.modal-footer .form-button-row button.btn{background-color:#007bff;font-size:10px}.modal-footer .form-button-row button.btn:hover{background-color:#0069d9}.modal-footer .col-sm-7{width:60%;float:right}.modal-footer .row.border-top{width:100%;float:left}.modal-footer .col-sm-5{width:40%!important;float:left!important;display:block!important}input,select,textarea{border-color:#ced4da!important}div#success-modal{position:fixed;top:0;left:0;z-index:1050;display:none;width:100%;height:100%;overflow:hidden;outline:0;transition:opacity .15s linear;display:block;background-color:#424242b0}.modal-dialog-submit{position:relative;display:-ms-flexbox;display:flex;-ms-flex-direction:column;flex-direction:column;width:100%;pointer-events:auto;background-color:#fff;background-clip:padding-box;border:1px solid rgba(0,0,0,.2);border-radius:.3rem;outline:0}div#success-modal .modal-content{background-color:#fff;width:100%;max-width:430px;position:absolute;margin:0 auto;left:0;right:0;top:40%;border-radius:7px;padding:16px;margin-top:10%}.modal-dialog-submit button.btn.btn-primary.pull-right{background-color:#007bff;padding:.375rem .75rem;margin-top:16px}.modal-footer .form-button-row button{letter-spacing:0;font-size:10px;line-height:1.2;padding:.2rem .3rem;max-width:120px;margin-right:5px;min-height:35px}.modal-footer .form-button-row button#custombox3{width:auto}.modal-footer .form-button-row button#custombox4{width:auto}.rightForm div#otherHobbiesId,.rightForm div#otherProfessionId{margin-top:10px}.modal-footer .row.border-top .col-sm-12.pt-2{padding-top:1px}.modal-footer .row.border-top .col-sm-12{padding:15px}.mainform input#name{background-image:none!important}#__lpform_name img#__lpform_name_icon{display:none!important}select{-webkit-appearance:menulist!important;appearance:textfield!important;background-image:none!important}.modal-footer .row.border-top span.text-general{color:#78909c;display:inline-block;font-size:11px}.modal-footer .row.border-top span.text-general a{color:#555}#personFormId .no-gutters{width:100%;float:left}.btn{font-size:14px}.modal-content .form-group .form-control,.modal-content .form-group label{font-size:13px}.modal-body{padding:1rem}.modal-footer{font-size:13px;padding-left:0;padding-right:0;position:relative;border-top:0;padding-top:0;display:block}.modal-footer label,.modal-footer p{line-height:18px}.modal-footer p.last{margin-bottom:0;padding-bottom:0}textarea.form-control.modal-form-control{height:70px}.box-white{background:#fff;border:1px solid #ccc;padding:15px;display:block;float:left;margin-bottom:5px;border-radius:5px;width:100%;box-shadow:none!important}.checkboxSpace,.radiobtnSpace,.radiobtnSpacemain{width:30px;display:inline-block;float:left}.userpic{width:70px;height:70px;border:1px solid #e6e6e6;display:inline-block;margin-right:13px;float:left}.userNameDes{display:inline-block;min-width:145px;width:55%;float:left}.userNameDes h4{font-size:14px;font-weight:700}.userNameDes p{line-height:1.5;font-size:13px;font-weight:400;color:#333;padding-bottom:0;margin-bottom:0}.modal-footer>:not(:first-child){margin-left:0;margin-right:0}.modal-footer>:not(:last-child){margin-right:-15px}.checkboxSpace{display:inline-block;position:relative;padding-left:24px;margin-bottom:12px;cursor:pointer;font-size:22px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.checkboxSpace input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.checkmark{position:absolute;top:0;left:0;height:20px;width:20px;background-color:#fff;border:2px solid #2196f3;border-radius:3px}.checkboxSpace:hover input~.checkmark{background-color:#2196f3}.checkboxSpace input:checked~.checkmark{background-color:#2196f3}.checkmark:after{content:"";position:absolute;display:none}.checkboxSpace input:checked~.checkmark:after{display:block}.checkboxSpace .checkmark:after{left:6px;top:3px;width:5px;height:10px;border:solid #fff;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}.radiobtnSpace{display:inline-block;position:relative;padding-left:24px;margin-bottom:12px;cursor:pointer;font-size:22px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.radiobtnSpace input{position:absolute;opacity:0;cursor:pointer;height:0;width:0}.radiomark{position:absolute;top:0;left:0;height:20px;width:20px;background-color:#fff;border:2px solid #2196f3;border-radius:3px}.radiobtnSpace:hover input~.radiomark{background-color:#2196f3}.radiobtnSpace input:checked~.radiomark{background-color:#2196f3}.radiomark:after{content:"";position:absolute;display:none}.radiobtnSpace input:checked~.radiomark:after{display:block}.radiobtnSpace .radiomark:after{left:6px;top:3px;width:5px;height:10px;border:solid #fff;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}.form-group.required .control-label:after{color:#d00;content:"*";position:reletive;margin-left:1px;top:1px}.footercheckbox{position:relative;width:auto}.smallbox{padding:5px;font-size:13px;height:70px;background:#2196f3;color:#fff;border-radius:5px}.modal-body label{margin-bottom:4px!important;color:#000}.modal-body .form-group{margin-bottom:0}.modal-body .form-control{height:30px;line-height:1}.form-button-row{display:block;margin-bottom:10px}.form-button-row .btn{letter-spacing:0;font-size:10px;line-height:1.2;padding:.2rem .3rem;max-width:120px;margin-right:5px;min-height:35px}#selectionview{margin-bottom:5px}.text-general{font-size:12px;margin-right:0}.footercheckbox .checkmark{top:2px}.main-form{width:100%}.modal-dialog-empow{max-width:800px;width:95%}.button-bottom{text-align:left;display:block;padding-right:10px;clear:both}.rightForm{width:97%}.form-button-row{margin-left:15px;margin-right:15px}.form-button-row button{margin-bottom:10px}.modal-md{max-width:450px}.modal-md .modal-footer{text-align:right;padding-right:20px}@media (min-width:1200px) and (max-width:1400px){.device-usage-chart .width-50-p{width:100%}}@media (max-width:1200px){.main-container{padding-left:0}.header-right{width:100%}.left-side-bar{left:-251px}.left-side-bar:before{display:block;opacity:0;visibility:hidden}.left-side-bar.open:before{opacity:1;visibility:visible;display:none}.header-right .brand-logo{display:table}.header-right .menu-icon{display:block}.footer-wrap{width:100%}}@media (min-width:991px){.modal-dialog-empow{width:370px!important}}@media (min-width:576px){.mainform{flex:none}.button-bottom{text-align:right}.rightForm{width:57.3333%!important}}@media (min-width:768px){.userNameDes{display:inline-block;min-width:170px;width:65%;float:left}.modal-dialog-empow{max-width:815px;width:25%}.rightForm{width:100%}.form-button-row{margin-left:0;margin-right:0}.button-bottom{text-align:right;clear:none}}@media (max-width:991px){.device-usage-chart .width-50-p{width:100%}#bd-example-modal-lg{height:100%;overflow:inherit;max-height:100%}#bd-example-modal-lg .modal-dialog-empow{top:60%}.modal-dialog-empow .mainform{width:100%!important;max-width:100%}.modal-body .col-sm-7.rightForm{margin-top:10px;width:100%!important}#bd-example-modal-lg .modal-dialog-empow{position:absolute;top:inherit;left:40%;transform:inherit;background:#fff;margin:20px auto;display:flex;height:500px;overflow:scroll;padding-bottom:48px!important}#bd-example-modal-lg .modal-dialog-empow.expand{left:0;right:0}.modal-footer .col-sm-7{width:100%}#bd-example-modal-lg .modal-dialog-empow{left:0;min-height:calc(100% - 2rem);right:0}.modal-dialog-empow{width:320px}.modal-content{width:100%}}@media (max-width:767px){#bd-example-modal-lg .modal-dialog-empow{left:0}.pre-loader{background-size:43%}.xs-pd-20-10{padding:20px 10px}.xs-mb-20{margin-bottom:20px}.h1,h1{font-size:2rem}.h2,h2{font-size:1.6rem}.h4,h4{font-size:1.4rem}.main-container{padding-bottom:0}.footer-wrap{position:relative;height:auto}.user-notification{margin-right:15px}.user-notification .dropdown-menu{width:100%;min-width:300px}.user-info-dropdown .dropdown-toggle .user-name{display:none}.page-header{margin-bottom:30px}.forgot-password{text-align:center}.ionicons-list li{width:16.6%}.timeline .timeline-date{position:relative;top:0;margin-bottom:20px;left:35px}.timeline ul:before{left:10px}.timeline ul li{padding-left:0;padding-bottom:0}.timeline ul li:before{left:2px;top:28px}.fc-toolbar .fc-left{float:none!important;padding-bottom:10px}.fc-toolbar .fc-left:after,.fc-toolbar .fc-right:after{content:"";clear:both;display:table}.fc-toolbar .fc-right{float:none!important;padding-bottom:10px}.faq-wrap .card-header .btn{font-size:17px}.modal-footer>:not(:last-child){margin-right:0}}@media (max-width:660px){.docs-buttons .btn-group-crop{display:block}}@media (max-width:360px){.modal-dialog-empow{width:98%}}@media (max-width:345px){.userpic{width:40px;height:40px}.userNameDes{width:100%}}';

        $css_code .= '#bd-float-modal-lg{display:block;position:fixed;width:100%;bottom:0;right:0;z-index:9999}#bd-float-modal-lg .modal-dialog-empow{position:relative;background:#fff;right:0;float:right;top:-150px}.popup-main{background-color:'.$style->bg_color.';border-radius:5px 0 0 5px}.popup-top-content h3,.popup-top-content p{color:#fff;margin-bottom:0}.popup-content-main{padding:0 20px}.modal-topbar-inner{display:flex;align-items:center;padding:20px 0;justify-content:center}.popup-top-content{margin-left:30px}.modal-footer label{color:#fff;font-size:15px}.form-content-main{padding:10px 0}.close-main{position:absolute;right:15px;top:0}.close-inner{color:#fff;font-size:15px;cursor:pointer}.company-text h5{color:#fff;margin:0}.arrow-img img{width:25px}.modal-bottom-bar-inner{display:flex;align-items:center;justify-content:space-between}.modal-bottom-bar{padding-top:10px;border-top:1px solid #fff;margin-top:10px}.arrow-img a,.submit-btn a{margin-top:10px;width:50px;height:50px;background-color:#000;display:flex;align-items:center;justify-content:center;border-radius:50px}.back-icon-inner{width:20px;transform:scale(-1)}.back-icon{position:absolute;left:15px;top:0;cursor:pointer}.bs-float-modal-lg,.form-control{width:100%}.mt-5{margin-top:5px}.mt-10{margin-top:10px}.ml-10{margin-left:10px}.step-2-text{width:30%}.askaq_checkbox{display:flex;align-items:center}.askaq>img{width:50px}.askaq_checkbox .form-check.askaq{margin:0 20px}.dropdown-main-row-1,.dropdown-main-row-2{display:flex}';
        return $css_code;
    }
}

if (!function_exists('addSnippetH')) {
    /**
     * @param $theme_id
     */
    function addSnippetH($theme_id, $style){
        try {
            \Log::info('-----------------------START :: addSnippet -----------------------');
            $shop = \Auth::user();

            $is_opt1 = ( $style->ask_a_que ) ? 'block' : 'none';
            $is_opt2 = ( $style->receive_a_phone ) ? 'block' : 'none';
            $is_opt3 = ( $style->schedule_video_call ) ? 'block' : 'none';
            $is_opt4 = ( $style->schedule_store_visit ) ? 'block' : 'none';
            $type = 'add';
            if ($type == 'add') {
                $value = <<<EOF
      {%if template contains "product"%}<script src="{{ 'customer_empowerment.js' | asset_url }}" defer="defer"></script>{{ "customer_empowerment.css" | asset_url | stylesheet_tag }}<div class="fade bs-example-modal-lg" id="bd-example-modal-lg" style="display:none;"><div class="modal-dialog-empow" id="toggle"><div class="modal-content"><div class="modal-body"><div class="row"><div class="col-sm-5 mainform"><div class="form-group required"> <label class="col-xs-12 control-label">Your Question: </label><textarea id="question" name="question" class="form-control modal-form-control" rows="2" cols="30" placeholder="Type your question here"></textarea></div><div class="form-group required"> <label class="col-xs-12 control-label">Name: </label> <input type="text" id="name" name="name" class="form-control" placeholder="Please provide your name."></div><div class="form-group required"> <label class="col-xs-12 control-label">How would you like to receive your response: </label> <select name="preferredModeOfInformation" id="preferredModeOfInformation" class="form-control" onChange="modeOfCommunication(this)"><option value="-- Select --">-- Select --</option><option value="Email">Email</option><option value="SMS">SMS</option></select></div><div class="form-group required" id="emailId"> <label class="col-xs-12 control-label">Email: </label> <input type="text" id="email" name="email" class="form-control control" placeholder="Your email"></div><div class="form-group required" id="phoneId"> <label class="col-xs-12 control-label">Phone: </label> <input type="text" id="mobileNumber" name="mobileNumber" class="form-control" placeholder="Your phone number e.g. +1 123-456-7890"></div><div class="form-group required" id="facebookId"> <label class="col-xs-12 control-label">Facebook Id: </label> <input type="text" id="facebookId" name="facebookId" class="form-control" placeholder="Your facebook id"></div><div class="form-group required" style="margin-bottom: -13px;"> <label class="col-xs-12 control-label">Who would you like to answer your question</label></div><div class="form-group"> <label class="col-xs-12 control-label" style="font-size: 12px;">(Please select at least one option)</label></div><div class="form-group required"><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="retailerCSRToAnser"> <label class="custom-control-label" for="retailerCSRToAnser">Customer Service Representative</label></div></div><div class="form-group required"><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="brandCSRToAnser"> <label class="custom-control-label" for="brandCSRToAnser">Manufacturer\'s Brand Representative</label></div></div><div class="form-group required"><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="expertOpinion"> <label class="custom-control-label" for="expertOpinion">Expert Opinion</label></div></div><div class="form-group required"><div class="custom-control custom-checkbox"> <input type="checkbox" class="custom-control-input" id="particularExistingCSRToAnser"> <label class="custom-control-label" for="particularExistingCSRToAnser">Verified Customer</label></div></div></div><div class="col-sm-7 rightForm"><div class="row" id="ourCustomFormFourth" data-id="particularExistingCSRToAnser" style="display:none;"><div class="col-sm-12"> <label class="col-xs-10 control-label" style="font-size: 10px;">If you would like the responding customer to match any of the following criteria, please specify below:</label><div style="margin-top: 0px"></div><div id="personFormId" style="display: block"><div class="row"><div class="col-sm-6 col-xs-12"><div class="form-group"> <label class="col-xs-12 control-label">Gender:</label> <select name="sex" id="sex" class="form-control"><option value="-- Select --">-- Select --</option><option value="Female">Female</option><option value="Male">Male</option> </select></div></div><div class="col-sm-6 col-xs-12"><div class="form-group"> <label class="col-xs-12 control-label">Age Group: </label> <select name="ageGroup" id="ageGroup" class="form-control"><option value="-- Select --">-- Select --</option><option value="20s">20s</option><option value="30s">30s</option><option value="40s">40s</option><option value="50s">50s</option><option value="60s">60s</option><option value="70s">70s</option><option value="80s">80s</option> </select></div></div></div><div class="form-group"> <label class="col-xs-12 control-label">Profession:</label> <select name="profession" id="profession" class="form-control"><option value="-- Select --">-- Select --</option><option value="Architect">Architect</option><option value="Computer/IT">Computer/IT</option><option value="Plumber">Plumber</option><option value="Electrician">Electrician</option><option value="Home Maker">Home Maker</option><option value="Business">Business</option><option value="Engineer">Engineer</option><option value="Other">Other</option> </select></div><div class="form-group" id="otherProfessionId"> <input type="text" id="otherProfession" name="otherProfession" class="form-control" placeholder="Please specifiy profession"></div><div class="form-group"> <label class="col-xs-12 control-label">Hobbies:</label> <select name="hobbies" id="hobbies" class="form-control"><option value="-- Select --">-- Select --</option><option value="Archery">Archery</option><option value="Backpacking">Backpacking</option><option value="Camping">Camping</option><option value="Cycling">Cycling</option><option value="Dancing">Dancing</option><option value="DIY">DIY</option><option value="Gaming">Gaming</option><option value="Gardening">Gardening</option><option value="Golfing">Golfing</option><option value="Hiking/Rock Climbing/Ice Climbing/Mountaineering">Hiking/Rock Climbing/Ice Climbing/Mountaineering</option><option value="Jogging/Training For Marathon">Jogging/Training For Marathon</option><option value="Martial Arts">Martial Arts</option><option value="Meditation">Meditation</option><option value="Mud Running">Mud Running</option><option value="Paintballing">Paintballing</option><option value="Sailing/Canoeing/Kayaking">Sailing/Canoeing/Kayaking</option><option value="Scuba diving/snorkeling">Scuba diving/snorkeling</option><option value="Skiing">Skiing</option><option value="Snow boarding">Snow boarding</option><option value="Surfing">Surfing</option><option value="Tennis">Tennis</option><option value="Travelling">Travelling</option><option value="Working out/weightlifting">Working out/weightlifting</option><option value="Yoga">Yoga</option><option value="Other">Other</option> </select></div><div class="form-group" id="otherHobbiesId"> <input type="text" id="otherHobbies" name="otherHobbies" class="form-control" placeholder="Please specifiy hobby"></div><div class="form-group"><div class="row"> <label class="col-xs-12 control-label" style="padding-left: 15px">Duration of Ownership:</label></div><div class="row no-gutters"><div class="col-sm-3"> <input type="number" id="noOfduration" name="noOfduration" class="form-control"></div><div class="col-sm-9"> <select name="durationOfOwnership" id="durationOfOwnership" class="form-control"><option value="-- Select --">-- Select --</option><option value="Days">Days</option><option value="Weeks">Weeks</option><option value="Months">Months</option><option value="Years">Years</option> </select></div></div></div><div class="form-group"> <label class="col-xs-12 control-label">Preferred location of the responder: </label> <input type="text" id="location" name="location" class="form-control" placeholder="City, State e.g. Boston, MA"></div></div></div></div></div></div></div><div class="modal-footer"><div class="row"><div class="col-sm-5"></div><div class="col-sm-7"><div class="form-button-row"><p id="selectionview">To edit or view previous selection, please click on the opton below:</p><p> <button type="button" class="btn btn-primary" id="custombox1" data-id="retailerCSRToAnser">Customer Service Representative</button> <button type="button" class="btn btn-primary" id="custombox2" data-id="brandCSRToAnser">Manufacturer Brand Representative</button> <button type="button" class="btn btn-primary" id="custombox3" data-id="expertOpinion">Expert Opinion</button> <button type="button" class="btn btn-primary" id="custombox4" data-id="particularExistingCSRToAnser">Verified Customer</button></p></div></div></div><div class="row border-top"><div class="col-sm-12 pt-2"> <label class="checkboxSpace footercheckbox"> <input type="checkbox" id="term-condition"> <span class="checkmark"></span> <span class="text-general"> I have read and understood the <a href="https://kingeranalytics.com/privacy" target="_blank"> privacy statement </a>. <span style="color: red">*</span> </span> </label><p class="last button-bottom"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="button" id="queryForm" class="btn btn-primary" data-dismiss="modal">Submit</button></p></div></div></div></div></div></div><div class="fade bs-example-modal-sm" id="success-modal" style="display:none;"><div class="modal-dialog-submit" role="document"><div class="modal-content"><div class="modal-body text-center font-18"><h3 class="mb-20">Submitted!</h3><div class="mb-30 text-center"> <img src="https://kingeranalytics-demo.com/img/success.png"></div> Your Query is submitted successfully!!</div><div class="modal-footer justify-content-center"> <button type="button" data-dismiss="modal" class="btn btn-primary pull-right" margin-right="15px">Done</button></div></div></div></div>
      
      <div class="fade bs-float-modal-lg" id="bd-float-modal-lg" style="display:none;"> <div class="modal-dialog-empow" id="toggle"> <div class="modal-content popup-main"> <div class="modal-header"> <div class="row"> </div></div><div class="modal-body"> <div class="row"> </div></div><div class="modal-footer"> <div class="row"> <div class="col-sm-12 pt-2"> <div class="popup-content-main"> <div class="modal-topbar"> <div class="modal-topbar-inner"> <div class="back-icon" style="display: none;" data-back="0" data-curr="0"> <img class="back-icon-inner" src="https://ucarecdn.com/10754b3e-05ec-4994-97c0-3c717d9dfa34/" alt=""/> </div><div class="popup-logo"> <a href="#"> <img src="$style->logo" alt=""/> </a> </div><div class="popup-top-content"> <h3 class="float-title">$style->title</h3> <p class="float-sub-title">Ask Anything to anyone.</p></div><div class="close-main"> <h3 class="close-inner" id="float_close">X</h3> </div></div></div><div class="modal-content"> <div class="modal-content-inner"> <form class="home step-0 other"> <div class="screen-1-class"> <div class="screen-label"> <label>Tell us, what would you like to do:</label> </div><div class="form-content-main"> <div class="form-check" style="display: $is_opt1"> <input class="form-check-input home_option" type="radio" name="question" id="ask_a_que" value="ask_a_que" checked> <label class="form-check-label" for="ask_a_que"> Ask a question </label> </div><div class="form-check" style="display: $is_opt2"> <input class="form-check-input home_option" type="radio" name="question" id="receive_phone" value="receive_phone"> <label class="form-check-label" for="receive_phone"> Receive a phone call </label> </div><div class="form-check" style="display: $is_opt3"> <input class="form-check-input home_option" type="radio" name="question" id="schedule_video" value="schedule_video"> <label class="form-check-label" for="schedule_video"> Schedule a video call </label> </div><div class="form-check" style="display: $is_opt4"> <input class="form-check-input home_option" type="radio" name="question" id="schedule_store_visit" value="schedule_store_visit"> <label class="form-check-label" for="schedule_store_visit"> Schedule a store visit </label> </div></div></div></form> <form class="receive_phone step-1 other" style="display: none;"> <div class="screen-1-class"> <div class="form-content-main"> <div class="form-group"> <textarea class="form-control float-que" id="float_question" cols="100" placeholder="Type your question here*"></textarea> </div><div class="form-group mb-3"> <input type="text" class="form-control float-unm" id="float_unm" placeholder="Your Full Name or FirstName or Nickname*" aria-label="Username" aria-describedby="basic-addon1"> </div><div class="form-group mb-3 mt-5"> <input type="email" class="form-control float-email" id="float_email" placeholder="Your email address*" aria-label="email" aria-describedby="basic-addon1"> </div><div class="form-group mb-3 mt-5"> <input type="number" class="form-control float-phone" id="float_phone" placeholder="Your phone number (mobile preferred)*" aria-label="phone" aria-describedby="basic-addon1"> </div></div></div></form> <form class="ask_a_que_s2 step-2 other" style="display: none;"> <div class="screen-1-class"> <div class="form-content-main"> <div class="form-group"> <label class="step-s2-label-1">who do you want to answer your question:*</label> <label class="step-s2-label-1">(Please select at least one option)</label> </div><div class="form-check"> <input class="form-check-input ask_a_que_option" type="radio" name="ans_que" data-value="CSR" id="csr" value="CSR"> <label class="form-check-label" for="csr"> Customer service Representative </label> </div><div class="form-check"> <input class="form-check-input ask_a_que_option" type="radio" name="ans_que" data-value="MBR" id="mbr" value="MBR"> <label class="form-check-label" for="mbr"> Manufacturer's Brand Representative </label> </div><div class="form-check"> <input class="form-check-input ask_a_que_option" type="radio" name="ans_que" data-value="expert" id="expert" value="expert"> <label class="form-check-label" for="expert"> Expert </label> </div><div class="form-check"> <input class="form-check-input ask_a_que_option" type="radio" name="ans_que" data-value="verified_customer" id="verified_customer" value="verified_customer"> <label class="form-check-label" for="verified_customer"> Verified Customer </label> </div><input type="hidden" value="CSR" class="ask_a_que_option_value"> </div></div></form> <form class="receive_phone_s2 step-2 other" style="display: none;"> <div class="screen-2-class"> <div class="form-content-main"> <div class="form-group"> <label class="step-2-label-1">Please give us two convenient days and times you are available to take the phone call</label> </div><div class="form-group mb-3"> <label>First Availability: </label> <input type="text" class="form-control step-2-text" placeholder="This week" aria-label="Username" aria-describedby="basic-addon1"> <input type="text" class="form-control step-2-text" placeholder="Day" aria-label="Username" aria-describedby="basic-addon1"> <input type="text" class="form-control step-2-text" placeholder="Time" aria-label="Username" aria-describedby="basic-addon1"> </div><div class="form-group mb-3 mt-10"> <label>Second Availability: </label> <input type="text" class="form-control step-2-text" placeholder="This week" aria-label="Username" aria-describedby="basic-addon1"> <input type="text" class="form-control step-2-text" placeholder="Day" aria-label="Username" aria-describedby="basic-addon1"> <input type="text" class="form-control step-2-text" placeholder="Time" aria-label="Username" aria-describedby="basic-addon1"> </div><div class="form-group mt-10"> <label class="step-2-label-2">You'll receive a confirmation msg & a 15 minutes before the call reminder text msg on your mobile phone.</label> </div></div></div></form> <form class="ask_a_que_s3 step-3 other" style="display: none;"> <div class="screen-3-class"> <div class="form-content-main"> </div><div class="verified_ppl_checkbox_main sub_other"> <div class="form-group mt-10"> <label class="step-5-label-1">If you'd like the responding customer to match any of the following criteria, please specify below:</label> <div class="dropdown"> <div class="row dropdown-main-row-1"> <div class="col-sm-6 col-xs-12" style="width: 100%;"> <div class="form-group"> <label class="col-xs-12 control-label">Gender:</label> <select name="floatsex" id="floatsex" class="form-control"> <option value="-- Select --">-- Select --</option> <option value="Female">Female</option> <option value="Male">Male</option> </select> </div></div><div class="col-sm-6 col-xs-12 ml-10" style="width: 100%;"> <div class="form-group"> <label class="col-xs-12 control-label">Age Group: </label> <select name="floatAgeGroup" id="floatAgeGroup" class="form-control"> <option value="-- Select --">-- Select --</option> <option value="20s">20s</option> <option value="30s">30s</option> <option value="40s">40s</option> <option value="50s">50s</option> <option value="60s">60s</option> <option value="70s">70s</option> <option value="80s">80s</option> </select> </div></div></div><div class="row dropdown-main-row-2 mt-10"> <div class="col-sm-6 col-xs-12" style="width: 100%;"> <div class="form-group"> <label class="col-xs-12 control-label">Profession:</label> <select name="floatProfession" id="floatProfession" class="form-control"> <option value="-- Select --">-- Select --</option> <option value="Architect">Architect</option> <option value="Computer/IT">Computer/IT</option> <option value="Plumber">Plumber</option> <option value="Electrician">Electrician</option> <option value="Home Maker">Home Maker</option> <option value="Business">Business</option> <option value="Engineer">Engineer</option> <option value="Other">Other</option> </select> </div><div class="form-group mt-10" id="floatOtherProfessionId" style="display: none;"> <input type="text" id="floatOtherProfession" name="floatOtherProfession" class="form-control" placeholder="Please specifiy profession" style="display: block;"> </div></div><div class="col-sm-6 col-xs-12 ml-10"> <div class="form-group"> <label class="col-xs-12 control-label">Hobbies:</label> <select name="floatHobbies" id="floatHobbies" class="form-control"> <option value="-- Select --">-- Select --</option> <option value="Archery">Archery</option> <option value="Backpacking">Backpacking</option> <option value="Camping">Camping</option> <option value="Cycling">Cycling</option> <option value="Dancing">Dancing</option> <option value="DIY">DIY</option> <option value="Gaming">Gaming</option> <option value="Gardening">Gardening</option> <option value="Golfing">Golfing</option> <option value="Hiking/Rock Climbing/Ice Climbing/Mountaineering">Hiking/Rock Climbing/Ice Climbing/Mountaineering</option> <option value="Jogging/Training For Marathon">Jogging/Training For Marathon</option> <option value="Martial Arts">Martial Arts</option> <option value="Meditation">Meditation</option> <option value="Mud Running">Mud Running</option> <option value="Paintballing">Paintballing</option> <option value="Sailing/Canoeing/Kayaking">Sailing/Canoeing/Kayaking</option> <option value="Scuba diving/snorkeling">Scuba diving/snorkeling</option> <option value="Skiing">Skiing</option> <option value="Snow boarding">Snow boarding</option> <option value="Surfing">Surfing</option> <option value="Tennis">Tennis</option> <option value="Travelling">Travelling</option> <option value="Working out/weightlifting">Working out/weightlifting</option> <option value="Yoga">Yoga</option> <option value="Other">Other</option> </select> </div><div class="form-group mt-10" id="floatOtherHobbiesId" style="display: none;"> <input type="text" id="floatOtherHobbies" name="floatOtherHobbies" class="form-control" placeholder="Please specifiy hobby" style="display: block;"> </div></div></div></div></div><div class="form-group mt-10"> <div class="row"> <label class="col-xs-12 control-label">Duration of Ownership:</label></div><div class="row no-gutters" style="display: flex;"> <div class="col-sm-3" style="width: 30%;"> <input type="number" id="floatNoOfduration" name="floatNoOfduration" class="form-control" max="12" min="1"></div><div class="col-sm-9 ml-10" style="width: 100%;"> <select name="floatDurationOfOwnership" id="floatDurationOfOwnership" class="form-control"> <option value="-- Select --">-- Select --</option> <option value="Days">Days</option> <option value="Weeks">Weeks</option> <option value="Months">Months</option> <option value="Years">Years</option> </select> </div></div></div><div class="form-group mt-10"> <label class="col-xs-12 control-label">Preferred location of the responder: </label> <input type="text" id="floatLocation" name="floatLocation" class="form-control" placeholder="City, State e.g. Boston, MA"> </div></div></div></form> <form class="ask_a_que_s4 step-4 other" style="display: none;"> <label class="step-4-label-1">How would you like to receive your response:* </label> <label class="step-4-label-1">(Please select at least one option)</label> <div class="form-content-main"> <div class="form-content-main"> <div class="form-check"> <input class="form-check-input response_option" type="radio" name="response_type" id="res_email" value="Email" checked> <label class="form-check-label" for="res_email"> Email (Display box asking for address) </label> </div><div class="form-check"> <input class="form-check-input response_option" type="radio" name="response_type" id="res_sms" value="SMS"> <label class="form-check-label" for="res_sms"> SMS (Display box asking for phone#) </label> </div><div class="form-check"> <input class="form-check-input response_option" type="radio" name="response_type" id="res_fb" value="fb"> <label class="form-check-label" for="res_fb"> Facebook (Ask for Facebook ID) </label> </div><div class="form-check"> <input class="form-check-input response_option" type="radio" name="response_type" id="res_twitter" value="tw"> <label class="form-check-label" for="res_twitter"> Twitter (Ask for Twitter handle) </label> </div></div><div class="form-group required response_opt" id="fEmailId" style="display: block;"> <label class="col-xs-12 control-label">Email: </label> <input type="text" id="responseEmail" name="email" class="form-control control" placeholder="Your email"> </div><div class="form-group required response_opt" id="fPhoneId" style="display: none;"> <label class="col-xs-12 control-label">Phone: </label> <input type="text" id="responseMobileNumber" name="mobileNumber" class="form-control" placeholder="Your phone number e.g. +1 123-456-7890"> </div><div class="form-group required response_opt" id="fFacebookId" style="display: none;"> <label class="col-xs-12 control-label">Facebook Id: </label> <input type="text" id="responseFacebookId" name="facebookId" class="form-control" placeholder="Your facebook id"> </div><div class="form-group required response_opt" id="fTwitterId" style="display: none;"> <label class="col-xs-12 control-label">Twitter Id: </label> <input type="text" id="responseTwitterId" name="twitterId" class="form-control" placeholder="Your twitter id"> </div></div></form> </div></div><div class="modal-bottom-bar"> <div class="modal-bottom-bar-inner"> <div class="company-text"> <h5>Powered by</h5> <h5>KINGER Analystic</h5> </div><div class="next-arrow-main"> <div class="next-arrow-main-inner"> <div class="arrow-img next-icon" data-next="1" data-curr="0"> <a href="#"><img src="https://ucarecdn.com/10754b3e-05ec-4994-97c0-3c717d9dfa34/" alt=""/></a> </div></div></div><div class="submit-btn-main" style="display:none;"> <div class="submit-btn-main-inner"> <div class="submit-btn"> <a href="#" id="queryFormFloat">Submit</a> </div></div></div></div></div></div></div></div></div></div></div></div>
      {%endif%}
EOF;
            }
            $parameter['asset']['key'] = 'snippets/customerEmpowerment.liquid';
            $parameter['asset']['value'] = $value;
            $asset = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);

            updateThemeLiquidH('customerEmpowerment', $theme_id);
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: addSnippet -----------------------');
            \Log::info(json_encode($e));
        }
    }
}

if (!function_exists('updateThemeLiquidH')) {
        /**
         * @param $snippet_name
         * @param $theme_id
         */
        function updateThemeLiquidH($snippet_name, $theme_id){
        try {
            \Log::info('-----------------------START :: updateThemeLiquidH -----------------------');
            $shop = \Auth::user();

            \Log::info('-----------------------START:: updateThemeLiquid-----------------------');
            $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',
                ["asset[key]" => 'layout/theme.liquid']);

            if (@$asset['body']['container']['asset']) {
                $asset = $asset['body']['container']['asset']['value'];
                \Log::info($snippet_name);
                \Log::info(!strpos($asset, "{% include '$snippet_name' %}"));
                // add after <body>
                if (!strpos($asset, "{% include '$snippet_name' %}")) {
                    $asset = str_replace('</body>', "{% include '$snippet_name' %}</body>", $asset);
                }

                $parameter['asset']['key'] = 'layout/theme.liquid';
                $parameter['asset']['value'] = $asset;
                $result = $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json', $parameter);
            }
            \Log::info(json_encode($asset));
        } catch (\Exception $e) {
            \Log::info('-----------------------ERROR :: updateThemeLiquidH -----------------------');
            \Log::info(json_encode($e));
        }
    }
}
