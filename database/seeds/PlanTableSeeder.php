<?php

use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert([
            [
                'id' => 1,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Basic"),
                'price' => env('PLAN_PRICE_1', 19),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Basic"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' =>2,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Basic"),
                'price' => env('PLAN_PRICE_1', 209),
                'interval' => 'ANNUAL',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Basic"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 3,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Advanced"),
                'price' => env('PLAN_PRICE_1', 49),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Advanced"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 4,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Advanced"),
                'price' => env('PLAN_PRICE_1', 549),
                'interval' => 'ANNUAL',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Advanced"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 5,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Premium"),
                'price' => env('PLAN_PRICE_1', 149),
                'interval' => 'EVERY_30_DAYS',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Premium"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
            [
                'id' => 6,
                'type' => env('IS_RECURRING', 'RECURRING'),
                'name' => env('PLAN_NAME_1', "Premium"),
                'price' => env('PLAN_PRICE_1', 909),
                'interval' => 'ANNUAL',
                'capped_amount' => 0.00,
                'terms' => env('PLAN_TERM_1', "Premium"),
                'trial_days' => env('TRIAL_DAY', 21),
                'test' => env('TEST_MODE', 1),
                'on_install' => env('ON_INSTALL', 1)
            ],
        ]);
    }
}
