<?php

use Illuminate\Support\Facades\Route;
URL::forceScheme('https');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify', 'check.plan'])->name('home');

Route::group(["middleware" => ["auth.shopify"]], function(){
    Route::get("/choose-plan", "Plan\PlanController@choosePlan")->name('choose-plan');
});

Route::group(['middl/var/www/html/shopify/app-order-tracking-biteware' => ['auth.shopify', 'check.plan']], function () {
    Route::get('dashboard', 'Dashboard\DashboardController@index')->name('dashboard');
    Route::post('dashboard.store', 'Dashboard\DashboardController@store')->name('dashboard.store');

//    plan routes
    Route::get('get-plan', 'Plan\PlanController@index')->name('get-plan');
    Route::post('change-plan', 'Plan\PlanController@changePlan')->name('change-plan');
    Route::get('change-plan-db', 'Plan\PlanController@changePlanDB')->name('change-plan-db');

// setting tab routes
    Route::get('ruleset/setting','Ruleset\RulesetController@index');
    Route::post('ruleset/setting','Ruleset\RulesetController@store');
    Route::get('ruleset/setting/change-status','Ruleset\RulesetController@changeStatus');

    //test routes
    Route::get('test','Test\TestController@test');
});


Route::get('flush', function(){
    request()->session()->flush();
});

//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
